<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Login</title>

        <!-- Compiled and minified CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">

        <!-- Compiled and minified JavaScript -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
    </head>

    <body>
        <div>

        </div>
        <div class="row">
            <div class="col s4 offset-s4">
                <div class="card blue-grey darken-1">
                    <div class="card-content white-text">
                        <span class="card-title">Login</span>
                        <?php if($_GET!=[]){  ?>                          
                            <span class="pink-text text-lighten-4">
                                <?php echo "$_GET[messege]"; ?>
                            </span>
                        <?php } ?>
                        <form action="validateSession.php" method="POST">
                            <label for="UserName">User </label>
                            <input type="text" id="UserName" name="user[userName]" placeholder="your userName">
                            
                            <label for="Password">Password</label>
                            <input type="password" id="Password" name="user[password]" placeholder="password">
                            
                            <button class="btn waves-effect waves-light" type="submit">
                                <i class="material-icons right">Submit</i>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div>

        </div>
    </body> 
</html>