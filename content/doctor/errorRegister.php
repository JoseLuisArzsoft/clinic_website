<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Error</title>
</head>
<body>
    <h1>Error of register</h1>
    <a href="home.php">Home</a>

    <?php
        include_once("../../libraries/doctor.php");
        $objUser=new Doctor();
        $objUser->closeSession('');

        if (array_key_exists( "Error Messege" ,$_SESSION)&& $_SESSION['Error Messege']!=''){
            $objUser->debug($_SESSION['Error Messege']);
        }
        session_write_close();
    ?>
</body>
</html>