<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script>
        //Method 3: javascript
        if (window.history.replaceState) { // verificamos disponibilidad
            window.history.replaceState(null, null, window.location.href);
        }
    </script>
    <title>Document</title>
    <?php
        include_once("../../libraries/doctor.php");
        $objUser=new Doctor();
        $objUser->closeSession('');
        include_once("../../style/materialize.html");
        if($_POST==[]){
            $_POST=$_SESSION['POSTDetails'];
        }else{
            if($_POST!=[]){
                $_SESSION['POSTDetails']=$_POST;
            }
        }
    ?>
</head>
<body>
    <?php 
        include_once('header.html');
    ?>
    <div class='row'>
        <div class='col s6 offset-s3'>
            <div class='card'>
                <div  class='card-content black-text'>
                    <span class='card-title'>
                        Appointment
                    </span>
                    <?php if(array_key_exists( "Error Messege" ,$_SESSION)&& $_SESSION['Error Messege']!=''){  ?>                          
                    <span class="pink-text text-lighten-2">
                        <?php 
                            echo "".$_SESSION['Error Messege']; 
                            $_SESSION['Error Messege']='';
                        ?>
                    </span>
                    <?php } ?>
                    <?php if(array_key_exists( "Update/Delete Error" ,$_SESSION)&& $_SESSION['Update/Delete Error']!=''){  ?>                          
                        <span class="pink-text text-lighten-2">
                            <?php 
                                echo "".$_SESSION['Update/Delete Error']; 
                                $_SESSION['Update/Delete Error']='';
                            ?>
                    </span>
                <?php } ?>
                    <p>
                        <?php
                            $objUser->detailsAppointment($_POST['Id']);
                        ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
    
</body>
</html>