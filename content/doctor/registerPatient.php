<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Register Patient</title>
        <?php 
            include_once("../../libraries/doctor.php");
            $objUser=new Doctor();
            $objUser->closeSession('');
            include_once("../../style/materialize.html");
        ?>
    </head>
    <body>
        <?php include_once("header.html"); ?>

    <div class="row">
        <div class="col s6 offset-s3">
            <div class="card">
                <div class="card-content black-text ">
                    <span class="card-title">
                        New Patient
                    </span>
                    <?php if(array_key_exists( "Error Messege" ,$_SESSION)&& $_SESSION['Error Messege']!=''){  ?>                          
                            <span class="pink-text text-lighten-2">
                                <?php 
                                    echo "".$_SESSION['Error Messege']; 
                                    $_SESSION['Error Messege']='';
                                ?>
                            </span>
                        <?php } ?>
                    <form action="validateNewPatient.php" method="POST">
                        <label for="name">Name: </label>
                        <input type="text" id="name" name="patient[Name]" placeholder="Name" minlength="5" required>

                        <?php $objUser->selectGender('')?>
                        <label id="room">Room</label>


                        <label for="birthday">Birthday </label>
                        <input type="date" id="birthday" name="patient[Birthday]" placeholder="Birthday" required>

                        <button class="btn waves-effect waves-light" type="submit" >Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>

    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
    <link rel="stylesheet" href="../css/main.css" />

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <script>
        $(document).ready(function() {
            $('select').material_select();
        });
    </script>
    </body>
</html>