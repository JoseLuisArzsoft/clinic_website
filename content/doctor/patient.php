<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Patient</title>
        <?php 
            include_once("../../libraries/doctor.php");
            $objUser=new Doctor();
            $objUser->closeSession('');
            include_once("../../style/materialize.html");
        ?>
    </head>
    <body>
        
        <?php include_once("header.html")?>
        <div class=row>
            <div class="col s8 offset-s2">
                <form action="patient.php" method="GET">
                    <div class="input-field">
                        <input id="search" name="word" type="text">
                        <CENTER>
                            <button class='btn waves-effect waves-light' type="submit">
                                    Search
                            </button>
                        </CENTER>
                    </div>
                </form>
            </div>
        </div>
        
        <div class="row">
            <div class="col s8 offset-s2">
                <div class="card">
                    <div class="card-content black-text ">
                        <span class="card-title">
                            Patient
                        </span>
                        <table class="striped">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <?php 
                                $objUser->showPatient($_GET);
                            ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>

    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
    <link rel="stylesheet" href="../css/main.css" />

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <script>
        $(document).ready(function() {
            $('select').material_select();
        });
    </script>
    </body>
</html>