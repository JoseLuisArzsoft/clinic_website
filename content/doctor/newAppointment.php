<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>New Appointment</title>
    <?php 
        include_once("../../libraries/doctor.php");
        $objUser=new Doctor();
        $objUser->closeSession('');
        include_once("../../style/materialize.html");
        $user=$_SESSION['user'];
    ?>
</head>
<body>
    <?php
        include_once("header.html");
        $patient=$_POST['patient'];
        if($_POST!=[]){
    ?>
            <div class="row">
                <div class="col s6 offset-s3">
                    <div class="card">
                        <div class="card-content black-text">
                            <span class="card-title">
                                New Appointment 
                            </span>
                            <?php if(array_key_exists( "Error Messege" ,$_SESSION)&& $_SESSION['Error Messege']!=''){  ?>                          
                            <span class="pink-text text-lighten-2">
                                <?php 
                                    echo "".$_SESSION['Error Messege']; 
                                    $_SESSION['Error Messege']='';
                                ?>
                            </span>
                            <?php } ?>
                            <div >
                                <span>
                                Name: <?php echo"$patient[Name]";?>
                                </span>
                            </div>
                            <div >
                                <span>
                                Doctor: <?php echo"$user[Name]"; ?>
                                </span>
                            </div>
                            <div >
                                <span>
                                Consulting Room: <?php echo"$user[Room]"; ?>
                                </span>
                            </div>
                            <div >
                                <span>
                                Gender: <?php echo"$patient[Gender]"; ?>
                                </span>
                            </div>
                            <form action="validateAppointment.php" method="POST">
                                <input type="hidden" name="appointment[PatientId]" value='<?php echo "$patient[Id]"?>' required>
                                <input type="hidden" name="appointment[DoctorId]" value= '<?php echo "$user[doctorId]"?>' required>
                                <input type="hidden" name="appointment[RoomId]" value= '<?php echo "$user[RoomId]"?>' required>

                                <label for="height">Height (cm)</label>
                                <input type="number" name="appointment[Height]" step="0.01" id="height" required>

                                <label for="weight">Weight (Kg)</label>
                                <input type="number" name="appointment[Weight]" step="0.01" id="weight" required>

                                <div class="row">
                                    <div class="input-field col s10">
                                        <textarea  name="appointment[Notes]" id="note" class="materialize-textarea">
    Diagnosed Disease: 
    Psymtoms: 
    Medicines: 

                                        </textarea>
                                        <label for="note"></label>
                                    </div>
                                </div>

                                <button class="btn waves-effect waves-light" type="submit">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
    <?php }else{
            header("Location: patient.php");
        }
    ?>
    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>

    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
    <link rel="stylesheet" href="../css/main.css" />

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <script>
        $(document).ready(function() {
            $('select').material_select();
        });
    </script>
</body>
</html>