<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Update Patientt</title>
    <script>
        //Method 3: javascript
        if (window.history.replaceState) { // verificamos disponibilidad
            window.history.replaceState(null, null, window.location.href);
        }
    </script>
    <?php 
        include_once("../../libraries/doctor.php");
        $objUser=new Doctor();
        $objUser->closeSession('');
        include_once("../../style/materialize.html");
        $user=$_SESSION['user'];
        if($_POST==[]){
            $_POST=$_SESSION['POSTDetails'];
        }else{
            if($_POST!=[]){
                $_SESSION['POSTDetails']=$_POST;
            }
        }
    ?>
</head>
<body>
    <?php
        include_once("header.html");
        $patient=$_POST['patient'];
    ?>
    <div class="row">
        <div class="col s6 offset-s3">
            <div class="card">
                <div class="card-content black-text">
                <span class="card-title">
                    Update Patient
                </span>
                <?php if(array_key_exists( "Error Messege" ,$_SESSION)&& $_SESSION['Error Messege']!=''){  ?>                          
                    <span class="pink-text text-lighten-2">
                        <?php 
                            echo "".$_SESSION['Error Messege']; 
                            $_SESSION['Error Messege']='';
                        ?>
                    </span>
                <?php } ?>
                <?php if(array_key_exists( "Update/Delete Error" ,$_SESSION)&& $_SESSION['Update/Delete Error']!=''){  ?>                          
                    <span class="pink-text text-lighten-2">
                        <?php 
                            echo "".$_SESSION['Update/Delete Error']; 
                            $_SESSION['Update/Delete Error']='';
                        ?>
                    </span>
                <?php } ?>
                    <form action="validateUpdatePatient.php" method="POST">
                        <input type="hidden" name="patient[Id]" value='<?php echo "$patient[Id]"?>' required>
                        <input type="text" name="patient[Name]" placeholder="Name" value='<?php echo "$patient[Name]"?>' required>
                        <input type="date" name="patient[Date]" placeholder="Date" value='<?php echo "$patient[Date]"?>' required>
                        <?php $objUser->selectGender($patient['Gender']);?>
                        <button class="btn waves-effect waves-light" type="submit">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>

    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
    <link rel="stylesheet" href="../css/main.css" />

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <script>
        $(document).ready(function() {
            $('select').material_select();
        });
    </script>
</body>
</html>