<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home</title>
    <?php 
        include_once("../../libraries/doctor.php");
        $objUser=new Doctor();
        $objUser->closeSession('');
        include_once("../../style/materialize.html");
        $user=$_SESSION['user']
    ?>
</head>
<body>
    <?php include_once("header.html");?>
    <br><br><br><br><br>
    <div class="row">
            <div class="col s6 offset-s3">
                <div class="card">
                    <div class="card-content black-text ">
                        <span class="card-title">
                            Doctor:
                        </span>
                        <div >
                            <span>
                            Name: <?php echo"$user[Name]"; ?>
                            </span>
                        </div>
                        <div >
                            <span>
                            User Name: <?php echo"$user[userName]"; ?>
                            </span>
                        </div>
                        <div >
                            <span>
                            Speciality: <?php echo"$user[Speciality]"; ?>
                            </span>
                        </div>
                        <div >
                            <span>
                            Start Time: <?php echo"$user[StartTime]"; ?>
                            </span>
                        </div>
                        <div >
                            <span>
                            End Time: <?php echo"$user[EndTime]"; ?>
                            </span>
                        </div>
                        <div >
                            <span>
                            Consulting Room: <?php echo"$user[Room]"; ?>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</body>
</html>