<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Consultin Room</title>
        <?php 
            include_once("../../libraries/administrator.php");
            $objUser=new Admisnitrator();
            $objUser->closeSession('');
            include_once("../../style/materialize.html");
        ?>
    </head>
    <body>
        <?php 
            include_once("header.html");
        ?>
        <div class=row>
            <div class="col s8 offset-s2">
                <form action="consultingRoom.php" method="GET">
                    <div class="input-field">
                        <input id="search" name="word" type="text">
                        <CENTER>
                            <button class='btn waves-effect waves-light' type="submit">
                                    Search
                            </button>
                        </CENTER>
                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col s6 offset-s3">
                <div class="card">
                    <div class="card-content black-text">
                        <span class="card-title">
                            Consulting Rooms
                        </span>
                        <?php if(array_key_exists( "Error Messege" ,$_SESSION)&& $_SESSION['Error Messege']!=''){  ?>                          
                            <span class="pink-text text-lighten-2">
                                <?php 
                                    echo "".$_SESSION['Error Messege']; 
                                    $_SESSION['Error Messege']='';
                                ?>
                            </span>
                        <?php } ?>
                        <?php if(array_key_exists( "Update/Delete Error" ,$_SESSION)&& $_SESSION['Update/Delete Error']!=''){  ?>                          
                            <span class="pink-text text-lighten-2">
                                <?php 
                                    echo "".$_SESSION['Update/Delete Error']; 
                                    $_SESSION['Update/Delete Error']='';
                                ?>
                            </span>
                        <?php } ?>
                        <table class="striped">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <!--Consulting Room-->
                            <?php
                                $objUser->showConsultigRoom($_GET);
                            ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>