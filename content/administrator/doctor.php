<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Doctor</title>
        <?php 
            include_once("../../libraries/administrator.php");
            $objUser=new Admisnitrator();
            $objUser->closeSession('');
            include_once("../../style/materialize.html");
        ?>
    </head>
    <body>
        <?php
            include_once("header.html");
        ?>
        <div class=row>
            <div class="col s8 offset-s2">
                <form action="doctor.php" method="GET">
                    <div class="input-field">
                        <input id="search" name="word" type="text">
                        <CENTER>
                            <button class='btn waves-effect waves-light' type="submit">
                                    Search
                            </button>
                        </CENTER>
                    </div>
                </form>
            </div>
        </div>

        <div class="row">
            <div class="col s8 offset-s2">               
                <div class="card">
                    <div class="card-content black-text">
                        <span class="card-title">
                            Doctors
                        </span>
                        <table class="striped">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>User Name</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <?php 
                                $objUser->showDoctor($_GET);
                            ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>