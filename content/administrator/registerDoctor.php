<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Register Doctor</title>
        <?php 
            include_once("../../libraries/administrator.php");
            $objUser=new Admisnitrator();
            $objUser->closeSession('');
            include_once("../../style/materialize.html");
        ?>
        <link rel="stylesheet" href="../../style/master.css">
    </head>
    <body>
        <?php include_once("header.html"); ?>

        <div class="row">
            <div class="col s6 offset-s3">
                <div class="card">
                    <div class="card-content black-text">
                        <span class="card-title">
                            New Doctor
                        </span>
                        <?php if(array_key_exists( "Error Messege" ,$_SESSION)&& $_SESSION['Error Messege']!=''){  ?>                          
                            <span class="pink-text text-lighten-2">
                                <?php 
                                    echo "".$_SESSION['Error Messege']; 
                                    $_SESSION['Error Messege']='';
                                ?>
                            </span>
                        <?php } ?>
                        <form action="validateNewDoctor.php" method="POST">
                            <label for="name">Name: </label>
                            <input type="text" id="name" name="user[Name]" minlength="4" required>
                            
                            <label for="userName">User Name: </label>
                            <input type="text" id="userName" name="user[UserName]"  minlength="6" required>

                            <div class=input-field col s12>
                                <select name="user[IdRoom]" id="room" required>
                                    <option value="" disabled selected>Choose your option</option>
                                    <option value="none">Other</option>
                                    <?php $objUser->selectRoom();?>
                                </select>
                                <label for="room">Consulting Room</label>
                            </div>
                            
                            <label for="speciality">Speciality: </label>
                            <input type="text" id="speciality" name="user[Speciality]"  minlength="5" required>

                            <label for="starttime">Start Time</label>
                            <input type="text" class='timepicker' id="starttime" name="user[StartTime]" required>

                            <label for="endtime">End Time</label>
                            <input type="text" class='timepicker' id="endtime" name="user[EndTime]" required>
                            
                            <label for="password">Password: </label>
                            <input type="password" name="user[Password]" id="password" minlength="8" maxlength="20" required>
                            
                            <button class="btn waves-effect waves-light" type="submit" >Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <script>
            document.addEventListener('DOMContentLoaded', function() {
            var elems = document.querySelectorAll('.timepicker');
            var instances = M.Timepicker.init(elems, {twelveHour: false});
            
            var elems = document.querySelectorAll('select');
            var instances = M.FormSelect.init(elems, null);

        });

        </script>
    </body>
</html>