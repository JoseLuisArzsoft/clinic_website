<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script>
        //Method 3: javascript
        if (window.history.replaceState) { // verificamos disponibilidad
            window.history.replaceState(null, null, window.location.href);
        }
    </script>
    <?php
        include_once("../../libraries/Administrator.php");
        $objUser=new Admisnitrator();
        $objUser->closeSession('');
        include_once("../../style/materialize.html");
        $user=$_SESSION['user'];
        if($_POST==[]){
            $_POST=$_SESSION['POSTResults'];
        }else{
            if($_POST!=[]){
                $_SESSION['POSTResults']=$_POST;
            }
        }
        $patient=$_POST['patient'];
    ?>
    <title>Result From: <?php echo"$patient[Name]";?></title>
</head>
<body>
    <?php
        include_once("header.html");
    ?>
    <div class="row">
        <div class="col s6 offset-s3">
            <div class="card">
                <div class="card-content black-text ">
                    <span class="card-title">
                        Appointments
                    </span>
                    <table class="striped">
                        <thead>
                            <tr>
                                <th>Consulting Room</th>
                                <th>Patient</th>
                                <th></th>
                            </tr>
                        </thead>
                        <!-- <th>Doctor</th> -->
                        <?php
                            $objUser->appointmentsFromPatient($patient['Id']);
                        ?>
                    </table>
                </div>
            </div>
        </div>
    </div>

</body>
</html>