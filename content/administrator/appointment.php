<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Appointments</title>
        <?php 
            include_once("../../libraries/administrator.php");
            $objUser=new Admisnitrator();
            $objUser->closeSession('');
            include_once("../../style/materialize.html");
        ?>
    </head>
    <body>
        <?php include_once("header.html");?>

        <div class=row>
            <div class="col s8 offset-s2">
                <form action="appointment.php" method="GET">
                    <div class="input-field">
                        <input id="search" name="word" type="text">
                        <CENTER>
                            <button class='btn waves-effect waves-light' type="submit">
                                    Search
                            </button>
                        </CENTER>
                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col s6 offset-s3">
                <div class="card">
                    <div class="card-content black-text">
                        <span class="card-title">
                            Appointments
                        </span>
                        <table class="striped">
                            <thead>
                                <tr>
                                    <th>Consulting Room</th>
                                    <th>Patient</th>
                                </tr>
                            </thead>
                            <!--<th>Doctor</th>-->
                            <?php         
                                $objUser->showApointments($_GET);
                            ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>