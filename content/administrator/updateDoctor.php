<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script>
        //Method 3: javascript
        if (window.history.replaceState) { // verificamos disponibilidad
            window.history.replaceState(null, null, window.location.href);
        }
    </script>
    <title>Update Patientt</title>
    <?php 
        include_once("../../libraries/doctor.php");
        $objUser=new Doctor();
        $objUser->closeSession('');
        include_once("../../style/materialize.html");
        $user=$_SESSION['user'];
        if($_POST==[]){
            $_POST=$_SESSION['POSTDetails'];
        }else{
            if($_POST!=[]){
                $_SESSION['POSTDetails']=$_POST;
            }
        }
    ?>
</head>
<body>
    <?php
        include_once("header.html");
        if($_POST!=[]){
    ?>
            <div class="row">
                <div class="col s6 offset-s3">
                    <div class="card">
                        <div class="card-content black-text">
                        <span class="card-title">
                            Update Patient
                        </span>
                        <?php if(array_key_exists( "Error Messege" ,$_SESSION)&& $_SESSION['Error Messege']!=''){  ?>                          
                            <span class="pink-text text-lighten-2">
                                <?php 
                                    echo "".$_SESSION['Error Messege']; 
                                    $_SESSION['Error Messege']='';
                                ?>
                            </span>
                        <?php } ?>
                        <?php if(array_key_exists( "Update/Delete Error" ,$_SESSION)&& $_SESSION['Update/Delete Error']!=''){  ?>                          
                            <span class="pink-text text-lighten-2">
                                <?php 
                                    echo "".$_SESSION['Update/Delete Error']; 
                                    $_SESSION['Update/Delete Error']='';
                                ?>
                            </span>
                        <?php } ?>
                            <?php $doc=$_POST['user'];?>
                            <form action='validateUpdateDoctor.php' method='POST'>
                                <input type='hidden' name='user[IdTableUser]' value='<?php echo "$doc[IdTableUser]"; ?>' required>
                                <input type='hidden' name='user[IdTableDoctor]' value='<?php echo "$doc[IdTableDoctor]"; ?>' required>
                                
                                <label for="name">Name</label>
                                <input type='text' name='user[Name]' id="name" value='<?php echo "$doc[Name]"; ?>' required>
                                
                                <label for="username">User Name</label>
                                <input type='text' name='user[UserName]' id="username" value='<?php echo "$doc[UserName]"; ?>' required>
                            
                                <div class="input-field col s12">
                                    <select name='user[RoomId]' id="roomid" required>
                                        <option value='<?php echo "$doc[RoomId]"; ?>'><?php echo "$doc[Room]"; ?></option>
                                        <?php $objUser->selectRoom();?>
                                    </select>
                                    <label for="roomid">Consulting Room</label>
                                </div>
                                
                                <label for="speciality">Speciality</label>
                                <input type='text' name='user[Speciality]' id="speciality" value='<?php echo "$doc[Speciality]"; ?>' required>
                                
                                <label for="starttime">Start Time</label>
                                <input type='text' class='timepicker' id="starttime" name='user[StartTime]' value='<?php echo "$doc[StartTime]"; ?>' required>
                                
                                <label for="endtime">End Time</label>
                                <input type='text' class='timepicker' id="endtime" name='user[EndTime]' value='<?php echo "$doc[EndTime]"; ?>' required>
                                
                                <label for="password">Password</label>
                                <input type='password' id="password" name='user[Password]' value='<?php echo "$doc[Password]"; ?>' required>
                                
                                <button class='btn waves-effect waves-light' type='submit'>Update</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
    <?php }else{
            header("Location: patient.php");
        }
    ?>
            <script>
            document.addEventListener('DOMContentLoaded', function() {
            var elems = document.querySelectorAll('.timepicker');
            var instances = M.Timepicker.init(elems, {twelveHour: false});
            
            var elems = document.querySelectorAll('select');
            var instances = M.FormSelect.init(elems, null);

        });

        </script>
</body>
</html>