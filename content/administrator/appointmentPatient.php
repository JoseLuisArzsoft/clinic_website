<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script>
        //Method 3: javascript
        if (window.history.replaceState) { // verificamos disponibilidad
            window.history.replaceState(null, null, window.location.href);
        }
    </script>
    <title>Document</title>
    <?php
        include_once("../../libraries/Administrator.php");
        $objUser=new Admisnitrator();
        $objUser->closeSession('');
        include_once("../../style/materialize.html");
        if($_POST==[]){
            $_POST=$_SESSION['POSTDetails'];
        }else{
            if($_POST!=[]){
                $_SESSION['POSTDetails']=$_POST;
            }
        }
    ?>
</head>
<body>
    <?php 
        include_once('header.html');
    ?>
    <div class='row'>
        <div class='col s6 offset-s3'>
            <div class='card'>
                <div  class='card-content black-text'>
                    <span class='card-title'>
                        Appointment
                    </span>
                    <p>
                        <?php
                            $objUser->detailsAppointment($_POST['Id']);
                        ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
    
</body>
</html>