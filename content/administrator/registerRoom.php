<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Register Consulting Room</title>
        <?php 
            include_once("../../libraries/administrator.php");
            $objUser=new Admisnitrator();
            $objUser->closeSession('');
            include_once("../../style/materialize.html");
        
        ?>
    </head>
    <body>
        <?php include_once("header.html"); ?>
        
        <div class="row">
            <div class="col s6 offset-s3">
                <div class="card">
                    <div class="card-content black-text">
                        <span class="card-title">
                            New Consulting Room
                        </span>
                        <?php if(array_key_exists( "Error Messege" ,$_SESSION)&& $_SESSION['Error Messege']!=''){  ?>                          
                            <span class="pink-text text-lighten-2">
                            <?php 
                                    echo "".$_SESSION['Error Messege']; 
                                    $_SESSION['Error Messege']='';
                                ?>
                            </span>
                        <?php } ?>
                        <form action="validateNewRoom.php" method="POST">
                            <label for="name">Name: </label>
                            <input type="text" id="name" name="user[Name]" placeholder="Name" minlength="5" required>

                            <button class="btn waves-effect waves-light" type="submit" >Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>