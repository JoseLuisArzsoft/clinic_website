<?php
include_once("dbHandler.php");
    class Generic extends DBHandler{
        
        protected function queryPatient($strWord){
            if($strWord!=[]){
                $strQuery="SELECT Id, Name, Birthday, Gender FROM patient 
                WHERE 
                    Name LIKE '%$strWord[word]%' OR 
                    Birthday LIKE '%$strWord[word]%' OR
                    Gender LIKE '$strWord[word]%' OR
                    Id LIKE '$strWord[word]'
                ORDER BY Name";
            }else{
                $strQuery="SELECT Id, Name, Birthday, Gender FROM patient ORDER BY Name";
            }
            $objResult=$this->queryDB($strQuery);
            return $objResult;
        }

        protected function queryDoctor($IdDoctor){
            $strQuery="SELECT 
            doctor.Name, 
            doctor.Speciality,
            doctor.StartTime,
            doctor.EndTime,
            consultingroom.Id AS 'RoomId',
            consultingroom.Name AS 'Room' 
            FROM user
            INNER JOIN doctor ON user.DoctorId = doctor.Id
            INNER JOIN consultingroom ON doctor.IdRoom=consultingroom.Id
            WHERE user.Id=$IdDoctor";
            $objResult=$this->queryDB($strQuery);
            if($objResult!=[]){
                session_start();
                while($objDoctor=$objResult->fetch_object()){
                    $_SESSION['user']['Name']="$objDoctor->Name";
                    $_SESSION['user']['Speciality']="$objDoctor->Speciality";
                    $_SESSION['user']['StartTime']="$objDoctor->StartTime";
                    $_SESSION['user']['EndTime']="$objDoctor->EndTime";
                    $_SESSION['user']['RoomId']="$objDoctor->RoomId";
                    $_SESSION['user']['Room']="$objDoctor->Room";
                }
                session_write_close();
            }
        }

        protected function queryUser($userName, $password){
            $strQuery="SELECT * FROM user WHERE UserName='$userName' AND Password='$password'";
            $objResult=$this->queryDB($strQuery);
            return $objResult;
        }

        protected function queryAppointments($strWord){##########################
            if($strWord!=[]){
                $strQuery="SELECT
                doctor.Name AS 'Doctor',
                appointment.Id AS 'Id',
                consultingroom.Name AS 'ConsultingRoom', 
                patient.Name AS 'Patient' 
                FROM appointment
                    INNER JOIN consultingroom
                    ON appointment.ConsultingRoom= consultingroom.Id
                    INNER JOIN patient
                    ON appointment.Patient=patient.Id
                    INNER JOIN doctor
                    ON appointment.Doctor=doctor.Id
                    WHERE 
                        patient.Name LIKE '%$strWord[word]%' OR 
                        consultingroom.Name LIKE '%$strWord[word]%' OR
                        appointment.Id LIKE '$strWord[word]'    
                    ORDER BY patient.Name";
            }else{
                $strQuery="SELECT
                doctor.Name AS 'Doctor',
                appointment.Id AS 'Id',
                consultingroom.Name AS 'ConsultingRoom', 
                patient.Name AS 'Patient' 
                    FROM appointment
                    INNER JOIN consultingroom
                    ON appointment.ConsultingRoom= consultingroom.Id
                    INNER JOIN patient
                    ON appointment.Patient=patient.Id
                    INNER JOIN doctor
                    ON appointment.Doctor=doctor.Id";
            }
            $objResult=$this->queryDB($strQuery);#
            return $objResult;
        }

        protected function listAppointment($objResult){
            if ($objResult!=[]){
                while($objAppointment=$objResult->fetch_object()){
                    echo "<tr>";
                    #echo "<td>$objAppointment->Id</td>";
                    echo "<td>$objAppointment->ConsultingRoom</td>";
                    echo "<td>$objAppointment->Patient</td>";
                    #echo "<td>$objAppointment->Doctor</td>";
                    echo "<form action='appointmentPatient.php' method='POST'>";
                    echo "<input type='hidden' name=Id value='$objAppointment->Id'>";############################
                    echo "<td><button class='btn waves-effect waves-light' type='submit'>More</button></td>";
                    echo "</form>";
                    echo "</tr>";
                }
            }
        }

        public function detailsAppointment($IdAppointment){
            #$this->debug($_SESSION['user']);
            $doctorUserId=$_SESSION['user']['doctorId'];
            $strQuery="SELECT
            medical.Id AS 'Id',
            medical.Date AS Date,
            medical.Height AS 'Height', 
            medical.Weight AS 'Weight',
            medical.Notes AS 'Notes',
            room_.Id AS 'RoomId',
            room_.Name AS 'ConsultingRoom',
            doctor_.Id AS 'DoctorId',
            doctor_.Name AS 'Doctor',
            patient_.Id AS 'PatientId',
            patient_.Name AS 'Patient',
            patient_.Birthday AS 'Birthday',
            patient_.Gender AS 'Gender'
            FROM appointment medical
                INNER JOIN consultingroom room_
                    ON medical.ConsultingRoom= room_.Id
                INNER JOIN doctor doctor_
                    ON medical.Doctor= doctor_.Id
                INNER JOIN patient patient_
                    ON medical.Patient=patient_.Id
            WHERE medical.Id=$IdAppointment";
            $objResult=$this->queryDB($strQuery);
            
            if($objResult!=[]){
                while($objAppointment=$objResult->fetch_object()){
                    echo "<label>Name: $objAppointment->Patient</label></br>";
                    echo "<label>Birthday: $objAppointment->Birthday</label></br>";
                    echo "<label>Gender: $objAppointment->Gender</label></br>";
                    echo "<label>Doctor: $objAppointment->Doctor</label></br>";
                    echo "<label>Date: $objAppointment->Date</label></br>";
                    echo "<label>Consulting Room: $objAppointment->ConsultingRoom</label></br>";
                    if($objAppointment->DoctorId==$doctorUserId){
                        echo"<form action='updateAppointment.php' method='POST'>";
                        echo"   <input type='hidden' name='appointment[Id]' value='$objAppointment->Id' required>";
                        echo"   <label for='height'>Height (cm)</label>";
                        echo"   <input type='number' name='appointment[Height]' value='$objAppointment->Height' step='0.01' id='height' required>";
                        echo"   <label for='weight'>Weight (Kg)</label>";
                        echo"   <input type='number' name='appointment[Weight]' value='$objAppointment->Weight' step='0.01' id='weight' required>";
                        echo"   <div class='row'>";
                        echo"        <div class='input-field col s10'>";
                        echo"            <textarea  name='appointment[Notes]' id='note' class='materialize-textarea' required>";
                        echo"$objAppointment->Notes";
                        echo"            </textarea>";
                        echo"                <label for='note'></label>";
                        echo"        </div>";
                        echo"    </div>";
                        echo"    <button class='btn waves-effect waves-light' type='submit'>Update</button>";
                        echo"</form>";
                    }else{
                        echo "<label>Height: $objAppointment->Height</label></br>";
                        echo "<label>Weight: $objAppointment->Weight</label><br>";
                        echo "<label>";
                        echo "<div class='card'>";
                        echo "<pre>$objAppointment->Notes</pre>";
                        echo" </div>";
                        echo "</label>";
                    }

                }
            }
        }

        public function appointmentsFromPatient($Id){
            $strQuery="SELECT appointment_.Id AS Id, 
                appointment_.Height AS Height,
                appointment_.Weight AS Weight,
                appointment_.Notes AS Notes,
                room_.Name AS ConsultingRoom, 
                doctor_.Name AS Doctor, 
                patient_.Name AS Patient 
                FROM appointment appointment_
                INNER JOIN consultingroom room_
                    ON appointment_.ConsultingRoom=room_.Id
                INNER JOIN doctor doctor_
                    ON appointment_.Doctor=doctor_.Id
                INNER JOIN patient patient_
                    ON appointment_.Patient=patient_.Id
                WHERE patient_.Id=$Id";#######################
            $objResult=$this->queryDB($strQuery);
            $this->listAppointment($objResult);
        }

        public function selectRoom(){
            $strQuery="SELECT Id, Name FROM consultingroom";

            $objResult=$this->queryDB($strQuery);
            if($objResult!=[]){
                while($objRoom=$objResult->fetch_object()){
                    echo"<option value='$objRoom->Id'>$objRoom->Name</option>";
                }
            }
        }

        protected function getIdAppointment(){
            $strQuery="SELECT MAX(Id) AS Id FROM appointment";
            $objResult=$this->queryDB($strQuery);

            if ($objResult!=[]) {
                while($objId = $objResult->fetch_object()){
                    return $objId->Id;
                }
            }else
                return '';
        }

        public function validateUser($user){
            $objResult=$this->queryUser($user['userName'], $user['password']);
            if($objResult!=[]){
                session_start();
                while($objUser=$objResult->fetch_object()){
                    $_SESSION['user']=array(
                        'Id'=>"$objUser->Id",
                        'userName'=>"$objUser->UserName",
                        'password'=>"$objUser->Password",
                        'typeUser'=>"$objUser->TypeUser",
                        'doctorId'=>"$objUser->DoctorId",
                    );
                }
                $arUser=$_SESSION['user'];
                $typeUser=$arUser['typeUser'];
                session_write_close();
                if($typeUser=='ADMINISTRATOR'){
                    header("Location: ./content/administrator/home.php");
                }else{
                    if($typeUser=='DOCTOR'){
                        $arDoctor=$this->queryDoctor($arUser['Id']);
                        header("Location: ./content/doctor/home.php");
                    }
                }
            }else{
                $messege="invalid user, please try again!";
                header("Location: index.php?messege=$messege");
            }
        }

        public function closeSession($option){
            session_start();
            if($option=='Yes'){
                session_unset();
            }
            if($_SESSION==array()){
                $this->debug($_SESSION);
                header("Location: ../../index.php");
                exit;
            }
        }
    }
?>