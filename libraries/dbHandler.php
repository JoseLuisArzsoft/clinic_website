<?php
/*
Project: Clinic
Author: José Luis Márquez Alvarado
Date: 2021/09/25
Versión: 1.0
Descripcion: Class that handles the database connection
*/
class DBHandler{
    private $strServername = "127.0.0.1";
    private $strUsername = "root";
    private $strPassword = "";
    private $strDBName = "clinic_jos";
    private $objConnection;

    /*
        Description: Method that starts a Database connection
        Author: Clases de Programacion
    */
    private function dbConnect(){
        $this->objConnection = new mysqli($this->strServername, $this->strUsername, $this->strPassword, $this->strDBName);

        // Check connection
        if ($this->objConnection->connect_error) {
            echo "Connection failed: " . $this->objConnection->connect_error;
            return false;
        }else{
            return true;
        }
    }

    /*
        Description: Method that executes a query to the Database
        Author: Clases de Programacion
    */
    protected function queryDB($strQuery){
        if ($this->dbConnect()) {
            $objResult = $this->objConnection->query($strQuery);
            
            if ($objResult && $objResult->num_rows > 0) {
                $this->objConnection->close();
                return $objResult;
            }else{
                if($this->objConnection->error){
                    echo "Query error: ".$this->objConnection->error;
                }
                $this->objConnection->close();
                return [];
            }
        }
        return [];
    }

    /*
        Description: Method that executes a insert query to the Database
        Author: Clases de Programacion
    */
    protected function insertDB($strQuery){
        if ($this->dbConnect()) {
            $objResult = $this->objConnection->query($strQuery);
            
            if ($objResult && $this->objConnection->insert_id) {
                $id = $this->objConnection->insert_id;
                $this->objConnection->close();
                return $id;
            }else{
                if($this->objConnection->error){
                    $errorMsg = "Insert error: ".$this->objConnection->error;
                    echo $errorMsg;
                    return $errorMsg;
                }
                $this->objConnection->close();
                return null;
            }
        }
        return null;
    }

    /*
        Description: Method that executes a delete query to the Database
        Author: Clases de Programacion
    */
    protected function updateDeleteDB($strQuery){
        if ($this->dbConnect()) {
            $objResult = $this->objConnection->query($strQuery);
            
            if ($objResult) {
                $this->objConnection->close();
                return TRUE;
            }else{
                if($this->objConnection->error){
                    $errorMsg = "Update/Delete error: ".$this->objConnection->error;
                    $this->objConnection->close();
                    return $errorMsg;
                }else{
                    $this->objConnection->close();
                    return FALSE;
                }
            }
        }
        return FALSE;
    }

    /*
        Description: Method to print a given input
        Author: Clases de Programacion
    */
    public function debug($input){
        echo "<br/><br/>";
        echo "<pre>Result: ".print_r($input, 1)."</pre>";
        echo "<br/><br/>";
    }
}
?>
