<?php
    include_once("generic.php");

    class Doctor extends Generic{
        public function showWorkday(){
            $strQuery="";
        }

        public function showPatient($strWord){
            $objResult=$this->queryPatient($strWord);
            echo "<tr>";
            echo "<td></td>";
            echo "<td></td>";
            echo "<td></td>";
            echo "<td><button class='btn-floating btn-large waves-effect waves-light red'><a href='registerPatient.php'><i class='material-icons'>add</i></a></button></td>";
            echo "</tr>";
            if ($objResult!=[]){
                while($objPatient=$objResult->fetch_object()){
                    echo "<tr>";
                    echo "<td>$objPatient->Name</td>";
                    echo "<form action='updatePatient.php' method='POST'>";
                    echo "    <input type='hidden' name='patient[Id]' value='$objPatient->Id' required>";
                    echo "    <input type='hidden' name='patient[Name]' value='$objPatient->Name' required>";
                    echo "    <input type='hidden' name='patient[Date]' value='$objPatient->Birthday' required>";
                    echo "    <input type='hidden' name='patient[Gender]' value='$objPatient->Gender' required>";;
                    echo "    <td><button class='btn waves-effect waves-light' type='submit'>Update</button></td>";#Esto debe ir en un formulario
                    echo "</form>";
                    echo "<form action='resultAppointment.php' method='POST'>";
                    echo "    <input type='hidden' name='patient[Id]' value='$objPatient->Id' required>";
                    echo "    <input type='hidden' name='patient[Name]' value='$objPatient->Name' required>";
                    echo "    <td><button class='btn waves-effect waves-light' type='submit'>Appointments</button></td>";#Esto debe ir en un formulario
                    echo "</form>";
                    echo "<form action='newAppointment.php' method='POST'>";
                    echo "    <input type='hidden' name='patient[Id]' value='$objPatient->Id' required>";
                    echo "    <input type='hidden' name='patient[Name]' value='$objPatient->Name' required>";
                    echo "    <input type='hidden' name='patient[Gender]' value='$objPatient->Gender' required>";
                    echo "    <td><button class='btn waves-effect waves-light' type='submit'>New Appointment</button></td>";#Esto debe ir en un formulario
                    echo "</form>";
                    echo "</form>";
                    echo "</tr>";
                }
            }
        }
        public function selectGender($value){
            echo "<td><select class='black-text' name='patient[Gender]' required>";
            if($value=="MEN"){
                echo "    <option value='$value'>Men</option>";
                echo "    <option value='WOMEN'>Women</option>";
                echo "    <option value='OTHER'>Other</option>";
            }else{
                if($value=="WOMEN"){
                    echo "    <option value='$value'>Women</option>";
                    echo "    <option value='MEN'>Men</option>";
                    echo "    <option value='OTHER'>Other</option>";
                }else{
                    if($value=="OTHER"){
                        echo "    <option value='$value'>Other</option>";
                        echo "    <option value='MEN'>Men</option>";
                        echo "    <option value='WOMEN'>Women</option>";
                    }else{
                        echo "    <option value='' disabled selected>Choose your option</option>";
                        echo "    <option value='MEN'>Men</option>";
                        echo "    <option value='WOMEN'>Women</option>";
                        echo "    <option value='OTHER'>Other</option>";
                    }
                }
            }
            echo "</select></td>";
        }
        
        public function showAppointment($strWord){
            $objResult=$this->queryAppointments($strWord);
            $this->listAppointment($objResult);
        }

        public function updatePatient($arPatient){
            $strQuery="UPDATE patient SET Name='$arPatient[Name]', Birthday='$arPatient[Date]', Gender='$arPatient[Gender]'
            WHERE Id=$arPatient[Id]";
            $objResult=$this->updateDeleteDB($strQuery);
            if(gettype($objResult)=="boolean" && $objResult==true){
                session_start();
                $_SESSION['Update/Delete Error']='';
                session_write_close();
                header("Location: ./patient.php?word=$arPatient[Id]");
            }else{
                session_start();
                $_SESSION['Update/Delete Error']="Error: The record wasn't updated, please try again. If problem persist, please reach out for support.";
                if(gettype($objResult)=="string"){
                    $_SESSION['Error Messege']=$objResult;
                }
                session_write_close();
                header('Location: ./updatePatient.php');###################################
            }
        }

        public function updateAppointment($arAppointment){
            $strQuery="UPDATE appointment SET Height='$arAppointment[Height]', Weight='$arAppointment[Weight]', Notes='$arAppointment[Notes]' WHERE Id=$arAppointment[Id]";
            $objResult=$this->updateDeleteDB($strQuery);
            if(gettype($objResult)=="boolean" && $objResult){
                session_start();
                $_SESSION['Update/Delete Error']='';
                session_write_close();
                header("Location: ./appointment.php?word=$arAppointment[Id]");
            }else{
                session_start();
                $_SESSION['Update/Delete Error']="Error: The record wasn't updated, please try again. If problem persist, please reach out for support.";
                if(gettype($objResult)=="string"){
                    $_SESSION['Error Messege']=$objResult;
                }
                session_write_close();
                header('Location: ./appointmentPatient.php');
            }
        }

        public function registerPatient($patient){#método para doctores
            $strQuery="INSERT INTO patient(Name, Birthday, Gender) VALUES ('$patient[Name]', '$patient[Birthday]', '$patient[Gender]')";
            $objResult=$this->insertDB($strQuery);
            session_start();
            if($objResult!=null){
                if (is_numeric($objResult)){
                    $_SESSION['Error Messege']='';
                    session_write_close();
                    header("Location: patient.php?word=$patient[Name]");
                }
                if(is_string($objResult)){
                    $_SESSION['Error Messege']="$objResult";
                    session_write_close();
                    header("Location: registerPatient.php");
                }
            }else{
                $_SESSION['Error Messege']="Could not validate patient data";
                    session_write_close();
                    header("Location: registerPatient.php");
            }
        }

        public function selectRoom(){
            $strQuery="SELECT Id, Name FROM consultingroom";

            $objResult=$this->queryDB($strQuery);
            if($objResult!=[]){
                while($objRoom=$objResult->fetch_object()){
                    echo"<option value='$objRoom->Id'>$objRoom->Name</option>";
                }
            }
        }

        public function registerAppointment($data){
            $strQuery="INSERT INTO appointment(Doctor, Patient, ConsultingRoom, Height, Weight, Notes) 
            VALUES ($data[DoctorId], $data[PatientId], $data[RoomId], $data[Height], $data[Weight], '$data[Notes]')";
            $objResult=$this->insertDB($strQuery);
            if($objResult!=null && is_numeric($objResult)){
                session_start();
                $_SESSION['Error Messege']='';
                $maxId=$this->getIdAppointment();
                if($maxId!=''){
                    session_write_close();
                    header("Location: appointment.php?word=$maxId");
                }else{
                    $_SESSION['Error Messege']="The medical appointment could not be registered";
                    header("Location: newAppointment.php");
                    session_write_close();
                }
            }else{
                session_start();
                $_SESSION['Error Messege']="The medical appointment could not be validated";
                session_write_close();
                header("Location: newAppointment.php");
                exit;
            }
        }
    }
?>