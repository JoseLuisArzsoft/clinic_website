<?php
    /*
    Project: 
    Author: José Luis Márquez Alvarado
    Date:
    Description:

    */
    include_once("generic.php");

    class Admisnitrator extends Generic{

        public function showDoctor($strWord){
            if ($strWord!=[]){
                $strQuery="SELECT
                doctor.Id AS 'IdTableDoctor', 
                doctor.Name AS 'Name',
                doctor.Speciality AS 'Speciality',
                doctor.StartTime AS 'StartTime',
                doctor.EndTime AS 'EndTime',
                consultingroom.Id AS 'RoomId',
                consultingroom.Name AS 'Room',
                user.Id AS 'IdTableUser',
                user.UserName AS 'UserName',
                user.Password AS 'Password'
                FROM user
                INNER JOIN doctor
                    ON user.DoctorId=doctor.Id      
                INNER JOIN consultingroom
                    ON doctor.IdRoom=consultingroom.Id
                    WHERE 
                        doctor.Name LIKE '%$strWord[word]%' OR 
                        doctor.Speciality LIKE '%$strWord[word]%' OR
                        user.UserName LIKE '%$strWord[word]%'
                    ORDER BY doctor.Name";
            }else{
                $strQuery="SELECT
                doctor.Id AS 'IdTableDoctor', 
                doctor.Name AS 'Name',
                doctor.Speciality AS 'Speciality',
                doctor.StartTime AS 'StartTime',
                doctor.EndTime AS 'EndTime',
                consultingroom.Id AS 'RoomId',
                consultingroom.Name AS 'Room',
                user.Id AS 'IdTableUser',
                user.UserName AS 'UserName',
                user.Password AS 'Password'
                FROM user
                INNER JOIN doctor
                    ON user.DoctorId=doctor.Id      
                INNER JOIN consultingroom
                    ON doctor.IdRoom=consultingroom.Id
                ORDER BY doctor.Name";
            }
            $objResult=$this->queryDB($strQuery);
            echo "<tr>";
            echo "<td></td>";
            echo "<td></td>";
            echo "<td></td>";
            echo "<td><button class='btn-floating btn-large waves-effect waves-light red'><a href='registerDoctor.php'><i class='material-icons'>add</i></a></button></td>";
            echo "</tr>";
            if ($objResult!=[]){
                while($objDoctor=$objResult->fetch_object()){
                    echo "<tr>";
                    echo "<td>$objDoctor->Name</td>";
                    echo "<td>$objDoctor->UserName</td>";
                    echo "<form action='updateDoctor.php' method='POST'>";
                    echo "<input type='hidden' name='user[IdTableUser]' value='$objDoctor->IdTableUser' required>";
                    echo "<input type='hidden' name='user[IdTableDoctor]' value='$objDoctor->IdTableDoctor' required>";
                    echo "<input type='hidden' name='user[Name]' value='$objDoctor->Name' required>";
                    echo "<input type='hidden' name='user[UserName]' value='$objDoctor->UserName' required>";
                    echo "<input type='hidden' name='user[RoomId]' value='$objDoctor->RoomId' required>";
                    echo "<input type='hidden' name='user[Room]' value='$objDoctor->Room' required>";
                    echo "<input type='hidden' name='user[Speciality]' value='$objDoctor->Speciality' required>";
                    echo "<input type='hidden' class='timepicker' name='user[StartTime]' value='$objDoctor->StartTime' required>";
                    echo "<input type='hidden' class='timepicker' name='user[EndTime]' value='$objDoctor->EndTime' required>";
                    echo "<input type='hidden' name='user[Password]' value='$objDoctor->Password' required>";
                    echo "<td><button class='btn waves-effect waves-light' type='submit'>Update</button></td>";
                    echo "</form>";

                    echo "<form action='deleteDoctor.php' method='POST'>";
                    echo "<input type='hidden' name='user[IdTableDoctor]' value='$objDoctor->IdTableDoctor' required>";
                    echo "<input type='hidden' name='user[IdTableUser]' value='$objDoctor->IdTableUser' required>";
                    echo "<td><button class='btn waves-effect waves-light' type='submit'>Delete</button></td>";
                    echo "</form>";
                    echo "</tr>";
                }
            }
        }

        public function showConsultigRoom($strWord){
            if($strWord!=[]){
                $strQuery="SELECT Id, Name FROM consultingroom 
                        WHERE Name LIKE '%$strWord[word]%'
                        ORDER BY Name";
            }else{
                $strQuery="SELECT Id, Name FROM consultingroom ORDER BY Name";
            }
            $objResult=$this->queryDB($strQuery);
            echo "<tr>";
            echo "<td></td>";
            echo "<td></td>";
            echo "<td><button class='btn-floating btn-large waves-effect waves-light red'><a href='registerRoom.php'><i class='material-icons'>add</i></a></button></td>";
            echo "</tr>";
            if($objResult!=[]){
                while($objRoom=$objResult->fetch_object()){
                    echo "<tr>";
                    echo "<form action='updateRoom.php' method=POST>";
                    echo "<input type='hidden' name='room[Id]' value='$objRoom->Id'>";
                    echo "<td><input type='text' name='room[Name]' value='$objRoom->Name'></td>";
                    echo "<td><button class='btn waves-effect waves-light' type='submit'>Rename</button></td>";#Esto debe ir en un formulario
                    echo "</form>";
                    echo "<form action='deleteRoom.php' method=POST>";
                    echo "<input type='hidden' name='room[Id]' value='$objRoom->Id'>";
                    echo "<td><button class='btn waves-effect waves-light' type='submit'>Delete</button></td>";#Esto debe ir en un formulario
                    echo "</form>";
                    echo "</tr>";
                }
            }
        }

        public function showPatient($strWord){
            $objResult=$this->queryPatient($strWord);
            if ($objResult!=[]){
                while($objPatient=$objResult->fetch_object()){
                    echo "<tr>";
                    echo "<td>$objPatient->Name</td>";
                    echo "<td>$objPatient->Birthday</td>";
                    echo "<td>$objPatient->Gender</td>";
                    echo "<form action='resultAppointment.php' method='POST'>";
                    echo "    <input type='hidden' name='patient[Id]' value='$objPatient->Id'>";
                    echo "    <input type='hidden' name='patient[Name]' value='$objPatient->Name'>";
                    echo "    <td><button class='btn waves-effect waves-light' type='submit'>Appointments</button></td>";#Esto debe ir en un formulario
                    echo "</form>";
                    echo "</tr>";
                }
            }
        }

        public function showApointments($strWord){
            $objResult=$this->queryAppointments($strWord);
            $this->listAppointment($objResult);
        }

        public function registerDoctor($doctor){
            $strQuery="INSERT INTO doctor(Name, Speciality, StartTime, EndTime, IdRoom) VALUES 
            ('$doctor[Name]', '$doctor[Speciality]', '$doctor[StartTime]', '$doctor[EndTime]', '$doctor[IdRoom]')";###################s
            $objResult=$this->insertDB($strQuery);
            if($objResult!=null && is_numeric($objResult)){
                $strQuery="SELECT Id, Name  FROM doctor WHERE Name='$doctor[Name]'";
                $objResult=$this->queryDB($strQuery);
                if($objResult!=[]){
                    $Id='';
                    while($objDoctor=$objResult->fetch_object()){
                        $Id="$objDoctor->Id";
                    }
                    $strQuery="INSERT INTO user(UserName, Password, TypeUser, DoctorId) 
                    VALUES('$doctor[UserName]','$doctor[Password]', 'DOCTOR', '$Id')";
                    $objResult=$this->insertDB($strQuery);
                    if($objResult!=null && is_numeric($objResult)){
                        session_start();
                        $_SESSION['Error Messege']="";
                        session_write_close();
                        header("Location: doctor.php?word=$doctor[Name]");

                    }else{
                        session_start();
                        $_SESSION['Error Messege']="The action could not validate the user access data";
                        session_write_close();
                        header("Location: registerDoctor.php");
                    }
                }else{
                    session_start();
                    $_SESSION['Error Messege']="The action failed to identify the doctor's name";
                    session_write_close();
                    header("Location: registerDoctor.php");
                }
            }else{
                session_start();
                $_SESSION['Error Messege']="The action could not be completed, try again!";
                session_write_close();
                header("Location: registerDoctor.php");
            }
            exit;
        }

        public function registerRoom($room){
            $strQuery="INSERT INTO consultingroom(Name) VALUES ('$room[Name]')";
            $objResult=$this->insertDB($strQuery);
            if($objResult!=null && is_numeric($objResult)){
                session_start();
                $_SESSION['Error Messege']='';
                session_write_close();
                header("Location: consultingRoom.php?word=$room[Name]");
            }else{
                session_start();
                $_SESSION['Error Messege']="Your consultingroom could not be registered";
                session_write_close();
                header("Location: registerRoom.php");
            }
        }

        public function updateDoctor($doctor){
            $strQuery="UPDATE doctor SET 
            Name='$doctor[Name]', 
            Speciality='$doctor[Speciality]',
            StartTime='$doctor[StartTime]',
            EndTime='$doctor[EndTime]',
            IdRoom='$doctor[RoomId]'
            WHERE Id=$doctor[IdTableDoctor]";
            $objResult=$this->updateDeleteDB($strQuery);
            if(gettype($objResult)=="boolean" && $objResult){
                $strQuery="UPDATE user SET UserName='$doctor[UserName]', Password='$doctor[Password]' WHERE Id=$doctor[IdTableUser]";
                $objResult=$this->updateDeleteDB($strQuery);
                if(gettype($objResult)=="boolean" && $objResult){
                    session_start();
                    $_SESSION['Update/Delete Error']='';
                    session_write_close();
                    header("Location: ./doctor.php?word=$doctor[Name]");
                }else{
                    session_start();
                    $_SESSION['Update/Delete Error']="Error: The record wasn't updated, please try again. If problem persist, please reach out for support.";
                    if(gettype($objResult)=="string"){
                        $_SESSION['Error Messege']=$objResult;
                    }
                    session_write_close();
                    header('Location: ./updateDoctor.php');
                }
            }else{
                session_start();
                $_SESSION['Update/Delete Error']="Error: The record wasn't updated, please try again. If problem persist, please reach out for support.";
                if(gettype($objResult)=="string"){
                    $_SESSION['Error Messege']=$objResult;
                }
                session_write_close();
                header('Location: ./updateDoctor.php');
            }
        }

        public function updateRoom($room){
            $strQuery="UPDATE consultingroom SET Name='$room[Name]' WHERE Id=$room[Id]";
            $objResult=$this->updateDeleteDB($strQuery);
            if($objResult){
                session_start();
                $_SESSION['Update/Delete Error']='';
                session_write_close();
                header("Location: ./consultingRoom.php?word=$room[Name]");
            }else{
                session_start();
                $_SESSION['Update/Delete Error']="Error: The record wasn't updated, please try again. If problem persist, please reach out for support.";
                if(gettype($objResult)=="string"){
                    $_SESSION['Error Messege']=$objResult;
                }
                session_write_close();
                header('Location: ./consultingRoom.php');
            }
        }

        public function deleteDoctor($Ids){
            $strQuery="DELETE FROM user WHERE Id=".$Ids['IdTableUser'];
            $objResult=$this->updateDeleteDB($strQuery);
            if(gettype($objResult) == "boolean" && $objResult){
                $strQuery="DELETE FROM doctor WHERE Id=".$Ids['IdTableDoctor'];
                $objResult=$this->updateDeleteDB($strQuery);
                if(gettype($objResult) == "boolean" && $objResult){
                    header('Location: doctor.php');
                    exit;#####
                }else{
                    header('Location: errorDelete.php');
                    exit;#####
                }
            }else{
                header('Location: errorDelete.php');
                exit;#####
            }
        }

        public function deleteRoom($IdRoom){
            $strQuery="DELETE FROM consultingroom WHERE Id=$IdRoom";
            $objResult=$this->updateDeleteDB($strQuery);
            if(gettype($objResult) == "boolean" && $objResult){
                header('Location: consultingRoom.php');
                exit;#####
            }else{
                header('Location: errorDelete.php');
                exit;#####
            }
        }
    }
?>