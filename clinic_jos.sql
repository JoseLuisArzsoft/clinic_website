-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.4.21-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Volcando estructura de base de datos para clinic_jos
CREATE DATABASE IF NOT EXISTS `clinic_jos` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `clinic_jos`;

-- Volcando estructura para tabla clinic_jos.appointment
CREATE TABLE IF NOT EXISTS `appointment` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Doctor` int(11) DEFAULT NULL,
  `Patient` int(11) DEFAULT NULL,
  `ConsultingRoom` int(11) DEFAULT NULL,
  `Height` float DEFAULT NULL,
  `Weight` float DEFAULT NULL,
  `Notes` longtext DEFAULT NULL,
  `Date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`Id`),
  KEY `FKConsultingRoomAppointment` (`ConsultingRoom`),
  KEY `FKPatientAppointment` (`Patient`),
  KEY `FKDoctorAppointment` (`Doctor`),
  CONSTRAINT `FKConsultingRoomAppointment` FOREIGN KEY (`ConsultingRoom`) REFERENCES `consultingroom` (`Id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FKDoctorAppointment` FOREIGN KEY (`Doctor`) REFERENCES `doctor` (`Id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FKPatientAppointment` FOREIGN KEY (`Patient`) REFERENCES `patient` (`Id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla clinic_jos.appointment: ~23 rows (aproximadamente)
/*!40000 ALTER TABLE `appointment` DISABLE KEYS */;
INSERT INTO `appointment` (`Id`, `Doctor`, `Patient`, `ConsultingRoom`, `Height`, `Weight`, `Notes`, `Date`) VALUES
	(1, 6, 1, 2, 171, 67, 'read', '2021-10-13 10:51:08'),
	(2, 7, 2, 3, 168, 70, 'blablabla', '2021-10-13 10:51:08'),
	(3, 8, 3, 4, 178, 83, 'blebleble', '2021-10-13 10:51:08'),
	(4, 9, 4, 5, 165, 59, 'bliblibli', '2021-10-13 10:51:08'),
	(5, 10, 5, 6, 160, 55, 'blobloblo', '2021-10-13 10:51:08'),
	(6, 11, 6, 7, 168, 58, 'blublublu', '2021-10-13 10:51:08'),
	(7, 1, 7, 8, 188, 70, 'lalala', '2021-10-13 10:51:08'),
	(8, 12, 8, 2, 171, 70, 'lelele', '2021-10-13 10:51:08'),
	(9, 13, 9, 3, 178, 70, 'lilili', '2021-10-13 10:51:08'),
	(10, 6, 10, 4, 167, 59, 'lololo', '2021-10-13 10:51:08'),
	(11, 7, 1, 5, 171, 70, NULL, '2021-10-13 10:51:08'),
	(12, 8, 2, 6, 168, 65, NULL, '2021-10-13 10:51:08'),
	(13, 9, 4, 7, 165, 62, NULL, '2021-10-13 10:51:08'),
	(14, 10, 4, 2, 165, 60, NULL, '2021-10-13 10:51:08'),
	(15, 11, 5, 3, 168, 68, NULL, '2021-10-13 10:51:08'),
	(16, 12, 6, 4, 168, 60, NULL, '2021-10-13 14:44:49'),
	(19, 1, 2, 2, 23, 170, NULL, '2021-10-13 15:02:47'),
	(20, 31, 23, 2, 170, 65, '    Diagnosed Disease: \r\n    Psymtoms: \r\n    Medicines: \r\n\r\n                                        ', '2021-10-13 15:22:46'),
	(21, 31, 32, 4, 180, 75, '    Diagnosed Disease: None None\r\n    Psymtoms: None\r\n    Medicines: None\r\n\r\n                                                    ', '2021-10-23 18:02:00'),
	(22, 31, 23, 19, 174.4, 65, '    Diagnosed Disease: None\r\n    Psymtoms: None\r\n    Medicines: None\r\n\r\n                                                    ', '2021-10-23 18:43:12'),
	(23, 31, 31, 6, 176, 70.2, '    Diagnosed Disease: None\r\n    Psymtoms: None\r\n    Medicines: None\r\n\r\n                                                    ', '2021-10-20 19:19:15'),
	(24, 31, 4, 3, 70, 69, '    Diagnosed Disease: Gripa\r\n    Psymtoms: Fiebre, Dolor de estomago\r\n    Medicines: Paracetamol\r\n\r\n                                        ', '2021-10-20 19:36:32'),
	(25, 31, 23, 6, 174, 68, '    Diagnosed Disease:  None\r\n    Psymtoms: None\r\n    Medicines: None\r\n\r\n                                        ', '2021-10-23 18:41:50');
/*!40000 ALTER TABLE `appointment` ENABLE KEYS */;

-- Volcando estructura para tabla clinic_jos.consultingroom
CREATE TABLE IF NOT EXISTS `consultingroom` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` text NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla clinic_jos.consultingroom: ~18 rows (aproximadamente)
/*!40000 ALTER TABLE `consultingroom` DISABLE KEYS */;
INSERT INTO `consultingroom` (`Id`, `Name`) VALUES
	(1, 'Radiology'),
	(2, 'General-1'),
	(3, 'General-2'),
	(4, 'General-3'),
	(5, 'General-4'),
	(6, 'General-5'),
	(7, 'General-6'),
	(8, 'Odontology-1'),
	(9, 'Odontology-2'),
	(10, 'Psychiatry'),
	(11, 'Toxicology'),
	(12, 'Endocrinology'),
	(13, 'General-7'),
	(14, 'Cardiology'),
	(15, 'Endocrinolgy'),
	(19, 'test1_consultingroom'),
	(22, 'General-7'),
	(23, 'General-8');
/*!40000 ALTER TABLE `consultingroom` ENABLE KEYS */;

-- Volcando estructura para tabla clinic_jos.doctor
CREATE TABLE IF NOT EXISTS `doctor` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` text NOT NULL,
  `Speciality` text NOT NULL,
  `StartTime` text NOT NULL,
  `EndTime` text NOT NULL,
  `IdRoom` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKRoom` (`IdRoom`),
  CONSTRAINT `FKRoom` FOREIGN KEY (`IdRoom`) REFERENCES `consultingroom` (`Id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla clinic_jos.doctor: ~9 rows (aproximadamente)
/*!40000 ALTER TABLE `doctor` DISABLE KEYS */;
INSERT INTO `doctor` (`Id`, `Name`, `Speciality`, `StartTime`, `EndTime`, `IdRoom`) VALUES
	(1, 'Juan José Torres Salas', 'Odontology', '02:25', '14:25', 8),
	(2, 'Pedro Juaquín Jimenez', 'Plastic Surgeon', '12:26', '15:27', 19),
	(3, 'Ximena Arrieta Betancourt ', 'Radiology', '03:29', '14:29', 2),
	(4, 'Aribeth Fuentesilla Domínguez', 'Cardiology', '01:15', '08:15', 3),
	(5, 'Gilberto Noel De Jesús Castañeda', 'Radiology', '02:19', '06:19', 4),
	(6, 'Alma Guerra Estrada', 'General', '18:14', '18:14', 5),
	(7, 'John Smith', 'General', '04:24', '18:24', 6),
	(8, 'Kareni Noemy Galindo Rodríguez', 'General', '06:26', '14:26', 6),
	(9, 'Albert Johnson', 'General', '05:13', '18:14', 7),
	(10, 'Ester Soliman Ibañez', 'General', '02:18', '07:18', 13),
	(11, 'Dojka Kim', 'General', '03:16', '07:16', 22),
	(12, 'Edmundo González Guerra', 'General', '07:16', '18:17', 23),
	(13, 'José Zapata Esequiel', 'General', '14:24', '19:24', 7),
	(14, 'Francisco Manuel Mariano ', 'Nurse', '06:19', '14:19', 23),
	(15, 'August Brown', 'Nurse', '16:15', '18:15', 15),
	(16, 'Emma Davis', 'Nurse', '10:17', '17:17', 15),
	(17, 'Guillermo Isamar Romero', 'Nurse', '03:20', '16:20', 2),
	(18, 'Teresa Gutierrez Del Carmen', 'Nurse', '08:27', '15:27', 2),
	(19, 'Mónica Leal Contreras', 'Nurse', '18:26', '01:26', 3),
	(20, 'William Rivera', 'Nurse', '19:29', '06:29', 13),
	(21, 'Alberto Galindo Silva', 'Nefrology', '16:14', '19:14', 9),
	(22, 'Daniel Román De Ocampo', 'Neumology', '03:16', '07:16', 9),
	(23, 'Irene Hall', 'Toxicology', '16:23', '20:23', 9),
	(24, 'Cristian Rivera Manzano', 'Alergology', '16:16', '19:16', 23),
	(25, 'Eva Nuñes Del Toro', 'Estomatology', '04:18', '09:18', 10),
	(26, 'Paulo Cordoba De la Cruz', 'Infectology', '04:26', '12:26', 19),
	(27, 'Efrain Ríos Samora', 'Psychiatry', '09:17', '14:17', 11),
	(28, 'Fatima Huerta Melendez', 'Endocrinology', '11:19', '20:19', 15),
	(29, 'Gustavo Diaz Telles', 'Endocrinology', '03:20', '13:21', 14),
	(30, 'Jarintzi Herrera Matamoros', 'Toxicology', '03:23', '15:23', 12),
	(31, 'test4321', 'test', '03:28', '15:28', 2),
	(33, 'test2_room', 'test', '06:28', '16:28', 4),
	(37, 'Hugo Ivan De Jesús Torres', 'General', '07:21', '11:21', 7),
	(38, 'Rafael Márquez Salgado', 'General', '07:27', '14:27', 3);
/*!40000 ALTER TABLE `doctor` ENABLE KEYS */;

-- Volcando estructura para tabla clinic_jos.patient
CREATE TABLE IF NOT EXISTS `patient` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` text NOT NULL,
  `Birthday` date NOT NULL,
  `Gender` text DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla clinic_jos.patient: ~33 rows (aproximadamente)
/*!40000 ALTER TABLE `patient` DISABLE KEYS */;
INSERT INTO `patient` (`Id`, `Name`, `Birthday`, `Gender`) VALUES
	(1, 'José Alfredo Gaona Huerta', '1996-04-11', 'MEN'),
	(2, 'Carlos Manuel Durán Montero', '1980-02-23', 'MEN'),
	(3, 'Federico Guzmán Hernan', '1990-04-30', 'MEN'),
	(4, 'Mayra Solís Zamora', '1994-07-20', 'WOMEN'),
	(5, 'Eva Cruz Cruz', '2001-03-27', 'WOMEN'),
	(6, 'Mariana Efraín Sotero', '1940-01-01', 'WOMEN'),
	(7, 'Daniela Colula García', '2000-01-26', 'WOMEN'),
	(8, 'José Luis Márquez Alvarado', '1899-01-01', 'MEN'),
	(9, 'Humberto Guzmán Hernán', '1996-02-03', 'MEN'),
	(10, 'Evelin Suarez Suarez', '1974-03-15', 'WOMEN'),
	(11, 'Hilario Portillo Diaz', '1950-06-07', 'MEN'),
	(12, 'Asguntín Solomé Hurtado', '1930-04-19', 'MEN'),
	(13, 'Yasmín Ignacio Iturbide', '1975-08-18', 'WOMEN'),
	(14, 'Josue Juarez Lizarraga', '1973-02-08', 'MEN'),
	(15, 'Marcelo Yumel Ximenez', '1992-06-03', 'MEN'),
	(16, 'Adolfo Gracielo Saltillo', '1998-04-23', 'MEN'),
	(17, 'Jimena Salazar ', '1999-12-12', 'MEN'),
	(18, 'Carmelita Ramiro Matamoros', '2001-01-01', 'MEN'),
	(19, 'Said Flores Carmona', '1993-05-29', 'MEN'),
	(20, 'Julio Ivan Atahuteno Francisco', '1980-03-09', 'MEN'),
	(21, 'David Leal Aparicio', '1997-08-24', 'MEN'),
	(22, 'Humberto Cuevas Leon', '1996-11-05', 'MEN'),
	(23, 'Adan Salinas Lopez', '1966-10-21', 'MEN'),
	(24, 'Florentina Molina Cuevas', '1998-02-28', 'WOMEN'),
	(25, 'Amadeo Calletano Hinojosa', '1950-04-13', 'MEN'),
	(26, 'Isaac Romero Romero', '1993-04-04', 'MEN'),
	(27, 'Carime Uriel Santos', '2001-01-08', 'WOMEN'),
	(28, 'Geronimo Guzmán Cortez', '1979-09-18', 'MEN'),
	(29, 'Damian Jimenez Lpoez', '2002-12-13', 'MEN'),
	(30, 'Guadalupe Ignacio Portillo', '1999-05-14', 'WOMEN'),
	(31, 'test', '1999-09-09', 'OTHER'),
	(32, 'Test Patient', '1999-05-20', 'WOMEN'),
	(33, 'Juan Adrián Manzano Zapata', '1999-12-12', 'MEN');
/*!40000 ALTER TABLE `patient` ENABLE KEYS */;

-- Volcando estructura para tabla clinic_jos.user
CREATE TABLE IF NOT EXISTS `user` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `UserName` text DEFAULT NULL,
  `Password` text DEFAULT NULL,
  `DoctorId` int(11) DEFAULT NULL,
  `TypeUser` text DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKDoctor_Id` (`DoctorId`),
  CONSTRAINT `FKDoctor_Id` FOREIGN KEY (`DoctorId`) REFERENCES `doctor` (`Id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla clinic_jos.user: ~35 rows (aproximadamente)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`Id`, `UserName`, `Password`, `DoctorId`, `TypeUser`) VALUES
	(1, 'Arqui_02', '12345678', NULL, 'ADMINISTRATOR'),
	(2, 'Juan_Jose', 'godofwar#45', 1, 'DOCTOR'),
	(3, 'Pedro', 'dfsedhrjhege5$', 2, 'DOCTOR'),
	(4, 'Ximena', 'nothing_Else_Metter90', 3, 'DOCTOR'),
	(5, 'Aribeth1', 'canguroKing_OGDM', 4, 'DOCTOR'),
	(6, 'GilbertoNJC', 'mortalCOmBatt&6', 5, 'DOCTOR'),
	(7, 'AlmaGuerra', 'PumPumStop#', 6, 'DOCTOR'),
	(8, 'J_Smith', 'PerejilConHuevo7', 7, 'DOCTOR'),
	(9, 'Kareny24', 'elLibroSalvaje67', 8, 'DOCTOR'),
	(10, 'Johnson1996', 'AlJohn200495', 9, 'DOCTOR'),
	(11, 'Ester_Soliñez', 'Soliman_LOve5', 10, 'DOCTOR'),
	(12, 'LectorOmniciente', 'caramelo portero', 11, 'DOCTOR'),
	(13, 'GuerraEd45', 'camaronCaramelo_ 7', 12, 'DOCTOR'),
	(14, 'Jose_Esequiel', 'spaceOxford56', 13, 'DOCTOR'),
	(15, 'Francisco69', 'SoloNoil34', 14, 'DOCTOR'),
	(16, 'Browney11', 'learn learn stop9', 15, 'DOCTOR'),
	(17, 'dSivaEmm', 'sacapuntasmovie$', 16, 'DOCTOR'),
	(18, 'IsaRomero12', 'whatisthis00!', 17, 'DOCTOR'),
	(19, 'Tere75_loop', 'coffeStartknow8', 18, 'DOCTOR'),
	(20, 'Mon_Leal', 'loveIsWar000547', 19, 'DOCTOR'),
	(21, 'RiveraDD1985', 'noLookspider9', 20, 'DOCTOR'),
	(22, 'BetoGalindo_01', 'silva_fighters8', 21, 'DOCTOR'),
	(23, 'RoDani_Ocampo', 'scrollNopvtv778', 22, 'DOCTOR'),
	(24, 'Hall_Irene92', 'nhyui}lkkggg088', 23, 'DOCTOR'),
	(25, 'Cristian005', 'aminomengañas34', 24, 'DOCTOR'),
	(26, 'EvaToro77', 'happyNocrown66', 25, 'DOCTOR'),
	(27, 'PauCordoba38', 'googleMaps3', 26, 'DOCTOR'),
	(28, 'SamEfrain_6', 'cricrikokoyu7', 27, 'DOCTOR'),
	(29, 'FatyMelendez20', '20gurtyu', 28, 'DOCTOR'),
	(30, 'GustavoKokoro', 'jijojkku44', 29, 'DOCTOR'),
	(31, 'JaryHerreraM_08', 'asdfgh123', 30, 'DOCTOR'),
	(32, 'test_01', 'test', 31, 'DOCTOR'),
	(33, 'test2_username', '12345678', 33, 'DOCTOR'),
	(37, 'Ivan123', 'ivan12312423', 37, 'DOCTOR'),
	(38, 'bichoSiuu', '12345678', 38, 'DOCTOR');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
